"""Test main module."""

import os

BASE_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
EXAMPLE_DIR = os.path.join(BASE_DIR, "examples")
