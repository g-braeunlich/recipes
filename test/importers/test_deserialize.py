"""Deserializer unit tests."""

import unittest
from datetime import timedelta
from fractions import Fraction

from gorps.importers.deserialize import primitive_dict
from gorps.model import AmountRange, Ingredient


class TestParseTimeDelta(unittest.TestCase):
    """parse_timedelta unit tests."""

    def test_parse_timedelta_parses_default_format(self) -> None:
        t = primitive_dict.parse_timedelta("00:01:03")
        self.assertEqual(t, timedelta(minutes=1, seconds=3))

    def test_parse_timedelta_parses_number_with_unit(self) -> None:
        t = primitive_dict.parse_timedelta("1 min")
        self.assertEqual(t, timedelta(minutes=1))
        t = primitive_dict.parse_timedelta("1 h")
        self.assertEqual(t, timedelta(hours=1))
        t = primitive_dict.parse_timedelta("0 sec")
        self.assertEqual(t, timedelta())

    def test_parse_timedelta_parses_number_with_unit_and_colon(self) -> None:
        t = primitive_dict.parse_timedelta("1:30:00 h")
        self.assertEqual(t, timedelta(hours=1, minutes=30))
        t = primitive_dict.parse_timedelta("1:30 h")
        self.assertEqual(t, timedelta(hours=1, minutes=30))
        t = primitive_dict.parse_timedelta("1:30 min")
        self.assertEqual(t, timedelta(minutes=1, seconds=30))

    def test_parse_timedelta_raises_error_for_invalid_format(self) -> None:
        with self.assertRaises(ValueError):
            primitive_dict.parse_timedelta("1")

        with self.assertRaises(ValueError):
            primitive_dict.parse_timedelta("1:30")

        with self.assertRaises(ValueError):
            primitive_dict.parse_timedelta("1:30:00 sec")

        with self.assertRaises(ValueError):
            primitive_dict.parse_timedelta("1:30 sec")

        with self.assertRaises(ValueError):
            primitive_dict.parse_timedelta("1:30:00 min")


class TestIngredient(unittest.TestCase):
    """tests for the Ingredient class."""

    def test_from_dict_amount_range(self) -> None:
        ingredient = primitive_dict.deserialize_ingredient(
            {
                "name": "I",
                "amount": {"min": 1.0, "max": 2},
                "unit": None,
            }
        )
        self.assertEqual(
            ingredient,
            Ingredient(
                name="I",
                amount=AmountRange(Fraction(1), Fraction(2)),
                unit=None,
                optional=False,
            ),
        )

    def test_from_dict_scalar_amount(self) -> None:
        ingredient = primitive_dict.deserialize_ingredient(
            {"name": "I", "amount": 1.0, "unit": None}
        )
        self.assertEqual(
            ingredient,
            Ingredient(name="I", amount=Fraction(1), unit=None, optional=False),
        )
