"""Unit tests for cooklang import."""

import hashlib
import os
import unittest
from dataclasses import replace
from fractions import Fraction

from gorps.content_types import COOKLANG
from gorps.content_types import cooklang as cooklang_fmt
from gorps.importers import cooklang
from gorps.model import Image, Ingredient, Recipe, Value
from test import EXAMPLE_DIR
from test.outputs.beans import RECIPE as EXPECTED_RECIPE

EXPECTED_INSTRUCTION = """Finely dice @onion{1} and briefly sauté in a #pan in @heat-resistant oil (rapeseed oil or olive oil){1%shot} together
with @finely diced bacon{125%g} and @clove of garlic{1}.

Add @salami or cabanossi{150%g}.

Add @white beans{1%can} and @kidney beans{1%can} and @strained tomatoes{250%ml}.

Season with @thyme{1%tsp}, @chili powder{1%tsp}, @pepper{1%tsp}, @paprika powder{1%tsp}, @salt{1%pinch} and @honey{2%tbsp}.

Serve with a #wooden spoon{}."""


class ImageMatcher(Image):
    """Matcher class for images."""

    def __init__(self, fmt: str, data_hash: str) -> None:
        self.fmt = fmt
        self.data_hash = data_hash

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Image):
            return False
        data_hash = hashlib.md5(other.data).hexdigest()  # noqa: S324
        return other.fmt == self.fmt and data_hash == self.data_hash


class TestCooklangImport(unittest.TestCase):
    """Unit tests for cooklang import."""

    def test_import_with_image(self) -> None:
        recipe = cooklang.load_recipe(
            os.path.join(
                EXAMPLE_DIR,
                "recipes",
                "cooklang",
                "Beans with Bacon a la Bud Spencer.cook",
            )
        )
        self.assertEqual(len(recipe.ingredients), len(EXPECTED_RECIPE.ingredients))
        expected_ingredients = [
            (
                replace(
                    ingredient,
                    unit=ingredient.unit.lower(),
                    name=ingredient.name.replace("Cabanossi", "cabanossi"),
                )
                if isinstance(ingredient, Ingredient) and ingredient.unit is not None
                else ingredient
            )
            for ingredient in EXPECTED_RECIPE.ingredients
        ]
        for r in recipe.ingredients:
            self.assertIn(r, expected_ingredients)
        expected_recipe = replace(
            EXPECTED_RECIPE,
            instruction=EXPECTED_INSTRUCTION,
            instruction_content_type=COOKLANG,
            ingredients=[],
            image=ImageMatcher(
                fmt="image/png", data_hash="8463a0c630314b4005ad9104641ab069"
            ),
        )
        recipe.ingredients = []
        self.assertEqual(recipe, expected_recipe)

    def test_import_without_image(self) -> None:
        recipe = cooklang.load_recipe(
            os.path.join(
                EXAMPLE_DIR,
                "recipes",
                "cooklang",
                "More Beans.cook",
            )
        )
        expected_recipe = Recipe(
            title="More Beans",
            instruction="""Cook beans.
Eat beans.

Ingredients:
- @Beans{1000%g}
- @Finely diced bacon{125%g}
- @Clove of garlic{1}
- @Salami or Cabanossi{150%g}""",
            instruction_content_type=COOKLANG,
            tags={"category": ["Main courses", "Starters", "Dessert"]},
            ingredients=[
                Ingredient(
                    name="Beans",
                    amount=Fraction(1000),
                    unit="g",
                ),
                *EXPECTED_RECIPE.ingredients[:3],
            ],
        )

        self.assertEqual(recipe, expected_recipe)

    def test_import_with_nutrition_labels(self) -> None:
        recipe = cooklang.load_recipe(
            os.path.join(
                EXAMPLE_DIR,
                "recipes",
                "cooklang",
                "Horse with Rösti.cook",
            )
        )
        expected_recipe = Recipe(
            title="Horse with Rösti",
            instruction="""Course:
- @Rösti{1%t}
- @Horse{1/2}""",
            instruction_content_type=COOKLANG,
            ingredients=[
                Ingredient(
                    name="Rösti",
                    amount=Fraction(1),
                    unit="t",
                ),
                Ingredient(name="Horse", amount=Fraction(1, 2)),
            ],
            nutrition_labels={"Energy Value": Value(Fraction(160000), "kcal")},
        )

        self.assertEqual(recipe, expected_recipe)


class TestMarkupRegion(unittest.TestCase):
    """tests for markup_items."""

    def test_markup_items_detects_single_word_at_eof(self) -> None:
        items = list(cooklang.markup_items("...@word", "@"))
        self.assertEqual(items, [("word", None)])

    def test_markup_items_detects_single_word_at_eol(self) -> None:
        items = list(cooklang.markup_items("...@word\n...{}", "@"))
        self.assertEqual(items, [("word", None)])

    def test_markup_items_detects_single_word_within_line(self) -> None:
        items = list(cooklang.markup_items("...@word ...", "@"))
        self.assertEqual(items, [("word", None)])

    def test_markup_items_detects_single_word_with_spec_at_eof(self) -> None:
        items = list(cooklang.markup_items("...@word{spec}", "@"))
        self.assertEqual(items, [("word", "spec")])

    def test_markup_items_detects_single_word_with_spec_at_eol(self) -> None:
        items = list(cooklang.markup_items("...@word{spec}\n...{}", "@"))
        self.assertEqual(items, [("word", "spec")])

    def test_markup_items_detects_single_word_with_spec_within_line(self) -> None:
        items = list(cooklang.markup_items("...@word{spec}...{}", "@"))
        self.assertEqual(items, [("word", "spec")])

    def test_markup_items_raises_value_error_for_unterminated_spec(self) -> None:
        with self.assertRaises(ValueError):
            list(cooklang.markup_items("...@word{...", "@"))
        with self.assertRaises(ValueError):
            list(cooklang.markup_items("...@word{...\n}", "@"))

    def test_markup_items_detects_multiple_words(self) -> None:
        items = list(
            cooklang.markup_items(
                "...@word1{spec1}...@word2{}...@word3 @word4{spec4}...", "@"
            )
        )
        self.assertEqual(
            items,
            [("word1", "spec1"), ("word2", None), ("word3", None), ("word4", "spec4")],
        )


class TestFormat(unittest.TestCase):
    """Tests for format_cooklang_body."""

    def test_format_cooklang_body_works_for_ingredients(self) -> None:
        body = """Then add @salt and @ground black pepper{} to taste."""
        expected_body = """Then add salt and ground black pepper to taste."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_ingredients_with_quantity(self) -> None:
        body = """Poke holes in @potato{2}."""
        expected_body = """Poke holes in potato."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_ingredients_with_quantity_and_unit(
        self,
    ) -> None:
        body = """Place @bacon strips{1%kg} on a baking sheet and glaze with @syrup{1/2%tbsp}."""
        expected_body = """Place bacon strips on a baking sheet and glaze with syrup."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_comments(self) -> None:
        body = """-- Don't burn the roux!

Mash @potato{2%kg} until smooth -- alternatively, boil 'em first, then mash 'em, then stick 'em in a stew."""
        expected_body = """
Mash potato until smooth -- alternatively, boil 'em first, then mash 'em, then stick 'em in a stew."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_block_comments(
        self,
    ) -> None:
        body = (
            """Slowly add @milk{4%cup} [- TODO change units to litres -], keep mixing"""
        )
        expected_body = """Slowly add milk , keep mixing"""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_cookware(
        self,
    ) -> None:
        body = """Place the potatoes into a #pot.
Mash the potatoes with a #potato masher{}."""
        expected_body = """Place the potatoes into a pot.
Mash the potatoes with a potato masher."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_timer(
        self,
    ) -> None:
        body = """Lay the potatoes on a #baking sheet{} and place into the #oven{}. Bake for ~{25%minutes}."""
        expected_body = """Lay the potatoes on a baking sheet and place into the oven. Bake for 25 minutes."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)

    def test_format_cooklang_body_works_for_timer_with_name(
        self,
    ) -> None:
        body = """Boil @eggs{2} for ~eggs{3%minutes}."""
        expected_body = """Boil eggs for 3 minutes."""
        formatted = cooklang_fmt.format_cooklang_body(body)
        self.assertEqual(formatted, expected_body)
