"""Unit tests for xml export."""

import unittest
from io import StringIO
from typing import Any, ClassVar
from xml.etree.ElementTree import canonicalize

from gorps.exporters import xml
from gorps.model import Image, Recipe
from test.outputs import png as png_data


class TestProcessXmlTemplate:
    """Base class for test."""

    source: str
    environment: ClassVar[dict[str, Any]]
    expected_output: str

    def test(self) -> None:
        processed = StringIO()
        xml.process_xml_template(self.source, self.environment, processed)
        unittest.TestCase().assertEqual(
            canonicalize(processed.getvalue(), strip_text=True),
            canonicalize(self.expected_output, strip_text=True),
        )


class TestSimpleControlStructure(TestProcessXmlTemplate, unittest.TestCase):
    """Simple control structures."""

    source = """
<root>
 <a v-if="show_a"/>
 <b v-for="item in items" v-bind:item="item"/>
</root>
"""
    environment: ClassVar[dict[str, Any]] = {"show_a": False, "items": ["1", "2", "3"]}
    expected_output = """
<root>
 <b item="1"/>
 <b item="2"/>
 <b item="3"/>
</root>
"""


class TestNestedFor(TestProcessXmlTemplate, unittest.TestCase):
    """Nested for loops."""

    source = """
<root>
 <a v-for="item in items" v-bind:id="item['id']">
   <sub v-for="subitem in item['subitems']" v-bind:id="subitem"/>
 </a>
</root>
"""
    environment: ClassVar[dict[str, Any]] = {
        "items": [
            {"id": "1", "subitems": ["1.1", "1.2"]},
            {"id": "2", "subitems": []},
            {"id": "3", "subitems": ["3.1"]},
        ],
    }
    expected_output = """
<root>
 <a id="1">
   <sub id="1.1"/>
   <sub id="1.2"/>
 </a>
 <a id="2">
 </a>
 <a id="3">
   <sub id="3.1"/>
 </a>
</root>
"""


class TestUnpackInFor(TestProcessXmlTemplate, unittest.TestCase):
    """Unpack tuples in for loop."""

    source = """
<root>
 <node v-for="key, val in items" v-bind:key="key" v-bind:val="val"/>
</root>
"""
    environment: ClassVar[dict[str, Any]] = {
        "items": [
            ("a", "1"),
            ("b", "2"),
            ("c", "3"),
        ],
    }
    expected_output = """
<root>
 <node key="a" val="1"/>
 <node key="b" val="2"/>
 <node key="c" val="3"/>
</root>
"""


class TestTemplateTag(TestProcessXmlTemplate, unittest.TestCase):
    """<template>."""

    source = """
<root>
 <template v-for="item in items">
  <a v-if="item['visible']" v-bind:id="item['id']"/>
  <b v-bind:id="item['id']"/>
 </template>
</root>
"""
    environment: ClassVar[dict[str, Any]] = {
        "items": [
            {"visible": True, "id": "1"},
            {"visible": False, "id": "2"},
            {"visible": True, "id": "3"},
        ],
    }
    expected_output = """
<root>
 <a id="1"/>
 <b id="1"/>
 <b id="2"/>
 <a id="3"/>
 <b id="3"/>
</root>
"""


class TestNamespaces(TestProcessXmlTemplate, unittest.TestCase):
    """E.g. fo namespace."""

    source = """
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
 <fo:block>
   Block
 </fo:block>
</fo:root>
"""
    environment: ClassVar[dict[str, Any]] = {}
    expected_output = source


class TestVBind(TestProcessXmlTemplate, unittest.TestCase):
    """v-bind."""

    source = """
<root>
 <a v-bind="attributes"/>
</root>
"""
    environment: ClassVar[dict[str, Any]] = {
        "attributes": {"id": "1", "class": "A"},
    }
    expected_output = """
<root>
 <a id="1" class="A"/>
</root>
"""


class TestInterpolation(TestProcessXmlTemplate, unittest.TestCase):
    """String interpolation."""

    source = """
<root>
 {{ text }}
</root>
"""
    environment: ClassVar[dict[str, Any]] = {
        "text": "...",
    }
    expected_output = """
<root>
 ...
</root>
"""


class TestConditionalInterpolation(TestProcessXmlTemplate, unittest.TestCase):
    """String interpolation combined with v-if."""

    source = """
<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
 <image v-if="recipe.image" v-bind="{'xlink:href': recipe.image.as_b64()}"/>
</svg>
"""
    environment: ClassVar[dict[str, Any]] = {
        "recipe": Recipe(
            title="Recipe",
            instruction="...",
            image=Image(fmt="image/png", data=png_data),
        ),
    }
    expected_output = """
<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
 <image xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAADElEQVQI12P4//8/AAX+Av7czFnnAAAAAElFTkSuQmCC"/>
</svg>
"""


class TestConditionalInterpolationElse(TestProcessXmlTemplate, unittest.TestCase):
    """String interpolation combined with v-if when condition is false."""

    source = TestConditionalInterpolation.source
    environment: ClassVar[dict[str, Any]] = {
        "recipe": Recipe(title="Recipe", instruction="..."),
    }
    expected_output = """
<svg xmlns="http://www.w3.org/2000/svg">
</svg>
"""
