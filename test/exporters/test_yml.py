"""Unit tests for yml export."""

import unittest
from fractions import Fraction
from io import StringIO

from gorps.exporters import yml
from gorps.model import AmountRange

from . import make_recipe


class TestYMLExport(unittest.TestCase):
    """Unit tests for yml export."""

    def test_export(self) -> None:
        with self.subTest("scalar amount"):
            buf = StringIO()
            yml.Exporter().export_stream_single(make_recipe(Fraction(2)), buf)
            self.assertEqual(buf.getvalue(), expected_yml.replace("{{ amount }}", " 2"))
        with self.subTest("fractional amount"):
            buf = StringIO()
            yml.Exporter().export_stream_single(make_recipe(Fraction(2, 3)), buf)
            self.assertEqual(
                buf.getvalue(), expected_yml.replace("{{ amount }}", " 2/3")
            )
        with self.subTest("range amount"):
            buf = StringIO()
            yml.Exporter().export_stream_single(
                make_recipe(AmountRange(Fraction(2), Fraction(3))), buf
            )
            self.assertEqual(
                buf.getvalue(),
                expected_yml.replace("{{ amount }}", "\n    min: 2\n    max: 3"),
            )


expected_yml = """---
title: Agavoni
instruction: 'Alle Zutaten im Rührglas gut rühren, anschliessend in das vorgekühlte
  Cocktailglas

  seihen und mit Orangenzeste dekorieren.'
instruction_content_type: text/plain
amount: 1
cookware: []
ingredients:
- name: Tequila white
  amount:{{ amount }}
  unit: cl
- name: Campari
  amount: 3
  unit: cl
- name: Vermouth rosso
  amount: 3
  unit: cl
- name: Orange Bitter
  amount: 2
  unit: Spritzer
- name: Garnitur
  ingredients:
  - name: Orangenzeste
    amount: 1
nutrition_labels: {}
tags:
  Glas: Cocktail Glass
  Zubereitung: In mixing glass
  category:
  - Tequila
  - Fancy Drinks
"""
