"""cooklang export tests."""

import glob
import os
import tempfile
import unittest
from io import StringIO

from gorps.exporters import cooklang
from gorps.exporters.templating import load_text_file
from gorps.importers import cooklang as cooklang_import
from test import EXAMPLE_DIR
from test.outputs.beans import RECIPE


def load_image(path: str) -> bytes:
    with open(path, "rb") as stream:
        return stream.read()


class TestCooklang(unittest.TestCase):
    """cooklang export tests."""

    def test_export_of_import_is_same_for_cooklang_recipes(self) -> None:
        for recipe_path in glob.glob(
            os.path.join(EXAMPLE_DIR, "recipes", "cooklang", "*.cook")
        ):
            with self.subTest(recipe=recipe_path):
                image_path = recipe_path.removesuffix(".cook") + ".png"
                expected_contents = load_text_file(recipe_path)
                try:
                    expected_image = load_image(image_path)
                except FileNotFoundError:
                    expected_image = None
                recipe = cooklang_import.load_recipe(recipe_path)
                with tempfile.TemporaryDirectory() as folder:
                    cooklang.Exporter().export_multifile([recipe], folder)
                    contents = load_text_file(
                        os.path.join(folder, os.path.basename(recipe_path))
                    )
                    image = (
                        load_image(os.path.join(folder, os.path.basename(image_path)))
                        if expected_image is not None
                        else None
                    )
                self.assertEqual(contents, expected_contents)
                self.assertEqual(image, expected_image)

    def test_export_of_non_cooklang_recipe_works(self) -> None:
        expected_body = """Finely dice the @onion{1} and briefly sauté in hot oil together with the bacon and garlic.

Add cabanossi / salami.

Add beans and @strained tomatoes{250%ml}.

Season with @thyme{1%tsp}, chili, @pepper{1%tsp}, paprika and @salt{1%Pinch}.

Serve with a #wooden spoon{}.

Ingredients:

- @Finely diced bacon{125%g}
- @Clove of garlic{1}
- @Salami or Cabanossi{150%g}
- @White beans{1%Can}
- @Kidney beans{1%Can}
- @Chili powder{1%tsp}
- @Paprika powder{1%tsp}
- @Heat-resistant oil (rapeseed oil or olive oil){1%Shot}
- @Honey{2%tbsp}

Cookware:

- #Pan{}

>> description: Chuck Norris? Never heard about her!
>> amount: 1
>> amount_unit: Pan
>> preparation_time: 15 min
>> cooking_time: 10 min
>> source: https://www.kabeleins.ch/sosiehtsaus/essen-trinken/rezepte/bohnen-mit-speck-nach-bud-spencer
>> link: https://www.kabeleins.ch/sosiehtsaus/essen-trinken/rezepte/bohnen-mit-speck-nach-bud-spencer
>> notes: Bud gives a damn about cream, but if you prefer, serve with cream!
>> category[]: Main courses
"""
        cooklang_body = StringIO()
        cooklang.Exporter().export_stream_single(RECIPE, cooklang_body)
        self.assertEqual(cooklang_body.getvalue(), expected_body)
