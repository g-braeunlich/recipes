"""Unit tests for the svg exporter."""

import os
from functools import reduce

from gorps.exporters.templating import load_text_file
from test import BASE_DIR

_template = load_text_file(os.path.join(BASE_DIR, "examples", "svg", "template.svg"))

_replacements = [
    ("{{recipes[0].tags['category'][0]}}", "Main courses"),
    ("{{recipes[1].tags['category'][0]}}", "Main courses"),
    ("{{recipes[2].tags['category'][0]}}", ""),
    ("{{recipes[3].tags['category'][0]}}", ""),
    ("{{recipes[0].title}}", "Beans with Bacon a la Bud Spencer"),
    ("{{recipes[1].title}}", "More Beans"),
    ("{{recipes[2].title}}", ""),
    ("{{recipes[3].title}}", ""),
    (
        "{{fmt_ingredients(recipes[0].ingredients)}}",
        """125 g Finely diced bacon
1 Clove of garlic
150 g Salami or Cabanossi
1 Onion
250 ml Strained tomatoes
1 Can White beans
1 Can Kidney beans
1 tsp Thyme
1 tsp Pepper
1 tsp Chili powder
1 tsp Paprika powder
1 Pinch Salt
1 Shot Heat-resistant oil (rapeseed oil or olive oil)
2 tbsp Honey""",
    ),
    (
        "{{fmt_ingredients(recipes[1].ingredients)}}",
        """1000 g Beans
125 g Finely diced bacon
1-2 Clove of garlic
150 g Salami or Cabanossi""",
    ),
    ("{{fmt_ingredients(recipes[2].ingredients)}}", ""),
    ("{{fmt_ingredients(recipes[3].ingredients)}}", ""),
    (
        "{{recipes[0].instruction}}",
        """Finely dice the onion and briefly sauté in hot oil together
with the bacon and garlic.

Add cabanossi / salami.

Add beans and strained tomatoes.

Season with thyme, chili, pepper, paprika and salt.
""",
    ),
    (
        "{{recipes[1].instruction}}",
        """Cook beans.
Eat beans.
""",
    ),
    ("{{recipes[2].instruction}}", ""),
    ("{{recipes[3].instruction}}", ""),
]
content = reduce(
    lambda text, replacement: text.replace(*replacement),
    _replacements,
    _template,
)
